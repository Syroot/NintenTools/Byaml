﻿using System;
using System.Collections.Generic;
using System.IO;
using Syroot.BinaryData;
using Syroot.Maths;

namespace Syroot.NintenTools.Byaml
{
    internal static class BinaryStreamExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static int Read3ByteInt32(this BinaryStream self)
        {
            byte[] bytes = self.ReadBytes(3);
            return self.ByteConverter.Endian == Endian.Little
                ? bytes[2] << 16 | bytes[1] << 8 | bytes[0]
                : bytes[0] << 16 | bytes[1] << 8 | bytes[2];
        }

        internal static List<ByamlPathPoint> ReadPath(this BinaryStream self, int length)
        {
            List<ByamlPathPoint> byamlPath = new List<ByamlPathPoint>(length);
            while (length-- > 0)
                byamlPath.Add(self.ReadPathPoint());
            return byamlPath;
        }

        internal static ByamlPathPoint ReadPathPoint(this BinaryStream self)
        {
            return new ByamlPathPoint
            {
                Position = self.ReadVector3F(),
                Normal = self.ReadVector3F(),
                Unknown = self.ReadInt32()
            };
        }

        internal static Vector3F ReadVector3F(this BinaryStream self)
        {
            return new Vector3F(self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
        }

        internal static uint ReserveOffset(this BinaryStream self)
        {
            uint position = (uint)self.Position;
            self.WriteInt32(0);
            return position;
        }

        internal static void SatisfyOffset(this BinaryStream self, uint offset, uint value)
        {
            using (self.TemporarySeek(offset, SeekOrigin.Begin))
                self.WriteUInt32(value);
        }

        internal static void Write3ByteInt32(this BinaryStream self, Int32 value)
        {
            byte[] bytes = new byte[sizeof(Int32)];
            self.ByteConverter.GetBytes(value, bytes);
            self.Write(bytes, self.ByteConverter.Endian == Endian.Little ? 0 : 1, 3);
        }

        internal static void WritePath(this BinaryStream self, List<ByamlPathPoint> value)
        {
            foreach (ByamlPathPoint point in value)
                self.WritePathPoint(point);
        }

        internal static void WritePathPoint(this BinaryStream self, ByamlPathPoint value)
        {
            self.WriteVector3F(value.Position);
            self.WriteVector3F(value.Normal);
            self.WriteInt32(value.Unknown);
        }

        internal static void WriteVector3F(this BinaryStream self, Vector3F value)
        {
            self.WriteSingle(value.X);
            self.WriteSingle(value.Y);
            self.WriteSingle(value.Z);
        }
    }
}
