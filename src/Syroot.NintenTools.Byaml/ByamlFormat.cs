﻿using Syroot.BinaryData;

namespace Syroot.NintenTools.Byaml
{
    /// <summary>
    /// Represents the contents of BYAML data.
    /// </summary>
    public struct ByamlFormat
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// The <see cref="BinaryData.Endian"/> determining the byte order in which multi-byte data is stored. This is
        /// typically the endianness of the target platform, e.g. <see cref="Endian.Little"/> for 3DS and Switch and
        /// <see cref="Endian.Big"/> for Wii U systems.
        /// </summary>
        public Endian Endian;

        /// <summary>
        /// A value indicating whether binary paths are supported. Required for Mario Kart 8 files.
        /// </summary>
        public bool SupportPaths;

        /// <summary>
        /// The (mostly arbitrary) revision of the BYAML format.
        /// </summary>
        public ushort Version;
    }
}
