﻿using System.IO;
using System.Xml.Linq;

namespace Syroot.NintenTools.Byaml.Scratchpad
{
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            string filePath = args[0];

            if (Path.GetExtension(filePath).ToUpper() == ".XML")
            {
                // Convert from XML to BYAML.
                XDocument xDocument = XDocument.Load(filePath, LoadOptions.None);
                dynamic root = ByamlData.FromXDocument(xDocument);

                string outFileName = Path.ChangeExtension(filePath, "byaml");
                ByamlData.Save(outFileName, root, ByamlData.GetFormat(xDocument));
            }
            else
            {
                // Convert from BYAML to XML.
                ByamlFormat format = ByamlData.GetFormat(filePath);
                dynamic root = ByamlData.Load(filePath);

                string outFileName = Path.ChangeExtension(filePath, "xml");
                ByamlData.ToXDocument(root, format).Save(outFileName);
            }
        }
    }
}
